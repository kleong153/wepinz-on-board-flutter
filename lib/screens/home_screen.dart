import 'package:flutter/material.dart';
import 'package:on_board_flutter/app_router.dart';
import 'package:on_board_flutter/constants/app_constants/app_constants.dart' as appConst;
import 'package:on_board_flutter/constants/shared_preference_keys.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// TODO Issue on this screen:
/// - Show confirm logout dialog before logout user.
/// - Go to my friend list screen when click My Friend List button.

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Future<_DataModel> _futureBuilder;
  final _DataModel _dataModel = _DataModel();

  Future<void> _onLogoutButtonPressedAsync() async {
    // Clear data and go to login screen.
    final sharedPref = await SharedPreferences.getInstance();

    await sharedPref.clear();

    Navigator.of(context).pushNamedAndRemoveUntil(AppRouter.login, (route) => false);
  }

  void _onMyFriendListButtonPressed() {
    // Which type of navigator to use?
    // Navigator.of(context).pushNamed(AppRouter.myFriendList);
    // Navigator.of(context).pushReplacementNamed(AppRouter.myFriendList);
    // Navigator.of(context).pushNamedAndRemoveUntil(AppRouter.myFriendList, (route) => false);
  }

  Future<_DataModel> _getScreenDataAsync() async {
    final sharedPref = await SharedPreferences.getInstance();

    _dataModel.userEmail = sharedPref.getString(SharedPreferenceKeys.userEmail) ?? '';

    return _dataModel;
  }

  @override
  void initState() {
    _futureBuilder = _getScreenDataAsync();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            tooltip: 'Logout',
            onPressed: _onLogoutButtonPressedAsync,
          ),
        ],
      ),
      body: FutureBuilder<_DataModel>(
        initialData: null,
        future: _futureBuilder,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasData) {
            return _content();
          }

          return const Center(
            child: Text('Error'),
          );
        },
      ),
    );
  }

  Widget _content() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(appConst.kDefaultPadding),
          child: Text('Hello ${_dataModel.userEmail}!'),
        ),
        Expanded(
          child: Center(
            child: ElevatedButton(
              child: const Text('My Friend List'),
              onPressed: _onMyFriendListButtonPressed,
            ),
          ),
        ),
      ],
    );
  }
}

class _DataModel {
  String userEmail = '';
}
